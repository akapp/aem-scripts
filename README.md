Automatic configuration with shell script
===========================================

### Author AEM configuration

#### Configuration process
1.	Download the script on the bitbucket repository as zip file: https://bitbucket.org/sveluz/aem-scripts/get/edf56d7b4677.zip
2.	Unzip the downloaded file: `unzip aem-scripts.zip`
3.	Change the access permission for the `aem-scripts/author` script: `chmod 755 aem-scripts/author`
4.	Modify the script parameters: `vi aem-scripts/author`

The script configuration is describes by these parameters:
>host=localhost
port=4502
admin=admin
password=admin
apachePort=80

>publishPort=4503
publishAdmin=admin
publishPassword=admin

>agentReplicationName=publish
agentReverseName=publish_reverse
agentFlushName=flush

>publishHost2=localhost
publishPort2=4503
publishAdmin2=admin
publishPassword2=admin

>agentReplicationName2=publish_2
agentReverseName2=publish_reverse_2

>publishHost3=localhost
publishPort3=4503
publishAdmin3=admin
publishPassword3=admin

>agentReplicationName3=publish_3
agentReverseName3=publish_reverse_3

If the `agentReplicationName`, `agentReverseName`, `agentFlushName`, `agentReplicationName2`, `agentReverseName2`, `agentReplicationName3`, `agentReverseName3` parameters are empty then the specific configuration of the agents is ignored.

5.	Run the script `./aem-scripts/author`

####	Results
The result for each command will be displayed on the shell window for each configuration step.

>Configuration report
>%%%%%%%%%% Geometrixx packages uninstall %%%%%%%%%%
>{"success":false,"msg":"no package"}
>%%%%%%%%%% Geometrixx package desinstall %%%%%%%%%%
>{"success":false,"msg":"no package"}
>%%%%%%%%%% Transport user %%%%%%%%%%
>configuration ok
>%%%%%%%%%% Transport password %%%%%%%%%%
>configuration ok
>%%%%%%%%%% Transport URI %%%%%%%%%%
>configuration ok
>%%%%%%%%%% Reverse user %%%%%%%%%%
>configuration ok
>%%%%%%%%%% Reverse password %%%%%%%%%%
>configuration ok
>%%%%%%%%%% Reverse URI %%%%%%%%%%
>configuration ok
>%%%%%%%%%% Fluh URI %%%%%%%%%%
>configuration ok
>%%%%%%%%%% Flush enabled %%%%%%%%%%
>configuration ok
>%%%%%%%%%% Transport agent creation 2 %%%%%%%%%%
>configuration ok
>%%%%%%%%%% Transport user 2 %%%%%%%%%%
>configuration ok
>%%%%%%%%%% Transport password 2 %%%%%%%%%%
>configuration ok
>%%%%%%%%%% Transport URI 2 %%%%%%%%%%
>configuration ok
>%%%%%%%%%% Transport name 2 %%%%%%%%%%
>configuration ok
>%%%%%%%%%% Reverse agent creation 2 %%%%%%%%%%
>configuration ok
>%%%%%%%%%% Reverse user 2 %%%%%%%%%%
>configuration ok
>%%%%%%%%%% Reverse password 2 %%%%%%%%%%
>configuration ok
>%%%%%%%%%% Reverse URI 2 %%%%%%%%%%
>configuration ok
>%%%%%%%%%% Reverse name 2 %%%%%%%%%%
>configuration ok
>%%%%%%%%%% Transport agent creation 3 %%%%%%%%%%
>configuration ok
>%%%%%%%%%% Transport user 3 %%%%%%%%%%
>configuration ok
>%%%%%%%%%% Transport password 3 %%%%%%%%%%
>configuration ok
>%%%%%%%%%% Transport URI 3 %%%%%%%%%%
>configuration ok
>%%%%%%%%%% Transport name 3 %%%%%%%%%%
>configuration ok
>%%%%%%%%%% Reverse agent creation 3 %%%%%%%%%%
>configuration ok
>%%%%%%%%%% Reverse user 3 %%%%%%%%%%
>configuration ok
>%%%%%%%%%% Reverse password 3 %%%%%%%%%%
>configuration ok
>%%%%%%%%%% Reverse URI 3 %%%%%%%%%%
>configuration ok
>%%%%%%%%%% Reverse name 3 %%%%%%%%%%
>configuration ok
>%%%%%%%%%%

In this case, the configuration is successful.

For the Geometrixx packages, the responses can be:
* `success:true` -> the command was executed with success.
* `success:false` but `msg:no package` -> the Geometrixx packages are already de-leted from CRX.

### Publish AEM configuration
####	Configuration process
1.	Download the script on the bitbucket repository as zip file: https://bitbucket.org/sveluz/aem-scripts/get/edf56d7b4677.zip
2.	Unzip the downloaded file: `unzip aem-scripts.zip`
3.	Change the access permission for the `aem-scripts/publish` script: `chmod 755 aem-scripts/publish`
4.	Modify the script parameters: `vi aem-scripts/publish`

The script configuration is describes by these parameters:
>host=localhost
>port=4502
>admin=admin
>password=admin
>apachePort=80
>
>agentFlushName=flush

If the `agentFlushName` parameters are empty then the specific configuration of the agents is ignored.

5.	Run the script: `./aem-scripts/publish`

#### Results
The result for each command will be displayed on the shell window for each configuration step.

>Configuration report
>%%%%%%%%%% Geometrixx packages uninstall %%%%%%%%%%
>{"success":false,"msg":"no package"}
>%%%%%%%%%% Geometrixx package desinstall %%%%%%%%%%
>{"success":false,"msg":"no package"}
>%%%%%%%%%% Fluh URI %%%%%%%%%%
>configuration ok
>%%%%%%%%%% Flush enabled %%%%%%%%%%
>configuration ok
>%%%%%%%%%% OSGi upload %%%%%%%%%%
>{"success":false,"msg":"Package already exists: /etc/packages/bit/bit-publish-osgi-configuration.zip"}
>%%%%%%%%%% OSGi install %%%%%%%%%%
>{"success":true,"msg":"Package installed"}
>%%%%%%%%%% CRX DE disabling %%%%%%%%%%
>{"fragment":false,"stateRaw":2}
>%%%%%%%%%% WebDav disabling %%%%%%%%%%
>{"fragment":false,"stateRaw":4}
>%%%%%%%%%% DavEx disabling %%%%%%%%%%
>{"fragment":false,"stateRaw":4}
>%%%%%%%%%%

In this case, the configuration is successful.

For the Geometrixx packages, the responses can be:

* `success:true` -> the command was executed with success.
* `success:false` but `msg:no package` -> the Geometrixx packages are already de-leted from CRX.

For the OSGi bundles, CRX DE, WebDav and DavEx, the state can be `2` or `4`.
